/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "upload_model.h"

#include <QDebug>
#include "metadata.h"
#include "uploader.h"

using namespace Uploader;

namespace Uploader {

class Upload
{
public:
    Upload(const QUrl &url, Imaginario::Metadata *metadata);

    QUrl url() const { return QUrl::fromLocalFile(m_filePath); }
    QString filePath() const { return m_filePath; }
    int progress() const { return m_progress; }
    UploadModel::UploadStatus status() const { return m_status; }
    QString photoId() const { return m_photoId; }
    QString title() const { return m_metadata.title; }
    QString description() const { return m_metadata.description; }
    QStringList tags() const { return m_metadata.tags; }

    void setTitle(const QString &title) { m_metadata.title = title; }
    void setDescription(const QString &description) {
        m_metadata.description = description;
    }
    void addTag(const QString &tag) {
        if (!m_metadata.tags.contains(tag)) m_metadata.tags.append(tag);
    }
    void removeTag(const QString &tag) { m_metadata.tags.removeOne(tag); }

    void setPhotoId(const QString &id) { m_photoId = id; }
    void setStatus(UploadModel::UploadStatus status) { m_status = status; }
    void setProgress(int progress) { m_progress = progress; }

private:
    QString m_filePath;
    Imaginario::Metadata::ImportData m_metadata;
    QString m_photoId;
    UploadModel::UploadStatus m_status;
    int m_progress;
};

class UploadModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(UploadModel)

public:
    UploadModelPrivate(UploadModel *q);
    ~UploadModelPrivate();

    void uploadNext();

private Q_SLOTS:
    void onProgressChanged();
    void onUploadFinished();

private:
    Uploader m_uploader;
    Imaginario::Metadata m_metadata;
    QHash<int, QByteArray> m_roles;
    QList<Upload> m_uploads;
    int m_currentUpload;
    bool m_isPublic;
    bool m_isFriends;
    bool m_isFamily;
    bool m_isHidden;
    bool m_isBusy;
    QStringList m_uploadedIds;
    mutable UploadModel *q_ptr;
};

} // namespace

Upload::Upload(const QUrl &url, Imaginario::Metadata *metadata):
    m_filePath(url.toLocalFile()),
    m_status(UploadModel::Ready),
    m_progress(0)
{
    metadata->readImportData(m_filePath, m_metadata);
}

UploadModelPrivate::UploadModelPrivate(UploadModel *q):
    QObject(q),
    m_currentUpload(-1),
    m_isPublic(false),
    m_isFriends(false),
    m_isFamily(false),
    m_isHidden(true),
    m_isBusy(false),
    q_ptr(q)
{
    m_roles[UploadModel::UrlRole] = "url";
    m_roles[UploadModel::ProgressRole] = "progress";
    m_roles[UploadModel::StatusRole] = "status";
    m_roles[UploadModel::TitleRole] = "title";
    m_roles[UploadModel::DescriptionRole] = "description";
    m_roles[UploadModel::TagsRole] = "tags";

    connect(&m_uploader, &Uploader::progressChanged,
            this, &UploadModelPrivate::onProgressChanged);
    connect(&m_uploader, &Uploader::finished,
            this, &UploadModelPrivate::onUploadFinished);
}

UploadModelPrivate::~UploadModelPrivate()
{
}

void UploadModelPrivate::uploadNext()
{
    Q_Q(UploadModel);

    if (m_uploader.isBusy()) return;

    m_currentUpload = -1;

    for (int i = 0; i < m_uploads.count(); i++) {
        if (m_uploads[i].status() == UploadModel::Ready) {
            m_currentUpload = i;
            break;
        }
    }

    if (m_currentUpload < 0) {
        // Nothing to do
        Q_EMIT q->finished();
        m_isBusy = false;
        Q_EMIT q->isBusyChanged();
        return;
    }

    Upload &upload = m_uploads[m_currentUpload];
    upload.setStatus(UploadModel::Uploading);
    m_uploader.uploadImage(upload.filePath(), upload.title(),
                           upload.description(), upload.tags(),
                           m_isPublic, m_isFriends, m_isFamily, m_isHidden);
}

void UploadModelPrivate::onProgressChanged()
{
    Q_Q(UploadModel);

    Q_ASSERT(m_currentUpload >= 0 && m_currentUpload < m_uploads.count());

    Upload &upload = m_uploads[m_currentUpload];
    upload.setProgress(m_uploader.progress());

    QModelIndex modelIndex = q->index(m_currentUpload, 0);
    Q_EMIT q->dataChanged(modelIndex, modelIndex);
}

void UploadModelPrivate::onUploadFinished()
{
    Q_Q(UploadModel);

    Q_ASSERT(m_currentUpload >= 0 && m_currentUpload < m_uploads.count());

    Upload &upload = m_uploads[m_currentUpload];
    if (!m_uploader.uploadedPhotoId().isEmpty()) {
        upload.setProgress(100);
        upload.setPhotoId(m_uploader.uploadedPhotoId());
        upload.setStatus(UploadModel::Uploaded);

        m_uploadedIds.append(m_uploader.uploadedPhotoId());
        Q_EMIT q->completedCountChanged();
    } else {
        upload.setProgress(0);
        upload.setStatus(UploadModel::Failed);
    }

    QModelIndex modelIndex = q->index(m_currentUpload, 0);
    Q_EMIT q->dataChanged(modelIndex, modelIndex);

    /* Remove completed uploads */
    if (upload.status() == UploadModel::Uploaded) {
        q->beginRemoveRows(QModelIndex(), m_currentUpload, m_currentUpload);
        m_uploads.removeAt(m_currentUpload);
        q->endRemoveRows();
    }

    uploadNext();
}

UploadModel::UploadModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new UploadModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
}

UploadModel::~UploadModel()
{
    delete d_ptr;
}

bool UploadModel::isBusy() const
{
    Q_D(const UploadModel);
    return d->m_isBusy;
}

int UploadModel::completedCount() const
{
    Q_D(const UploadModel);
    return d->m_uploadedIds.count();
}

QUrl UploadModel::organizerLink() const
{
    Q_D(const UploadModel);
    QUrl url("https://www.flickr.com/photos/organize");
    if (!d->m_uploadedIds.isEmpty()) {
        url = QUrl("https://www.flickr.com/photos/upload/edit/");
        url.setQuery(QStringLiteral("ids=") + d->m_uploadedIds.join(','));
    }
    return url;
}

void UploadModel::setPublic(bool value)
{
    Q_D(UploadModel);
    d->m_isPublic = value;
    Q_EMIT isPublicChanged();
}

bool UploadModel::isPublic() const
{
    Q_D(const UploadModel);
    return d->m_isPublic;
}

void UploadModel::setFriends(bool value)
{
    Q_D(UploadModel);
    d->m_isFriends = value;
    Q_EMIT isFriendsChanged();
}

bool UploadModel::isFriends() const
{
    Q_D(const UploadModel);
    return d->m_isFriends;
}

void UploadModel::setFamily(bool value)
{
    Q_D(UploadModel);
    d->m_isFamily = value;
    Q_EMIT isFamilyChanged();
}

bool UploadModel::isFamily() const
{
    Q_D(const UploadModel);
    return d->m_isFamily;
}

void UploadModel::setHidden(bool value)
{
    Q_D(UploadModel);
    d->m_isHidden = value;
    Q_EMIT isHiddenChanged();
}

bool UploadModel::isHidden() const
{
    Q_D(const UploadModel);
    return d->m_isHidden;
}

void UploadModel::login(const QByteArray &consumerKey,
                        const QByteArray &consumerSecret,
                        const QByteArray &token,
                        const QByteArray &tokenSecret)
{
    Q_D(UploadModel);
    qDebug() << "Login" << consumerKey << consumerSecret << token << tokenSecret;
    d->m_uploader.setKeys(consumerKey, consumerSecret,
                          token, tokenSecret);
}

void UploadModel::startUpload()
{
    Q_D(UploadModel);
    d->m_uploadedIds.clear();
    d->m_isBusy = true;
    Q_EMIT completedCountChanged();
    Q_EMIT isBusyChanged();
    d->uploadNext();
}

void UploadModel::addPhoto(const QUrl &url)
{
    Q_D(UploadModel);

    QModelIndex parent;
    int row = d->m_uploads.count();
    beginInsertRows(parent, row, row);
    d->m_uploads.append(Upload(url, &d->m_metadata));
    endInsertRows();
}

void UploadModel::clear()
{
    Q_D(UploadModel);

    beginResetModel();
    d->m_uploads.clear();
    endResetModel();
}

QVariant UploadModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

void UploadModel::setTitle(int row, const QString &title)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count()) return;
    Upload &u = d->m_uploads[row];
    u.setTitle(title);
    QModelIndex modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex);
}

void UploadModel::setDescription(int row, const QString &description)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count()) return;
    Upload &u = d->m_uploads[row];
    u.setDescription(description);
    QModelIndex modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex);
}

void UploadModel::addTag(int row, const QString &tag)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count() || tag.isEmpty()) return;
    Upload &u = d->m_uploads[row];
    u.addTag(tag.trimmed());
    QModelIndex modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex);
}

void UploadModel::removeTag(int row, const QString &tag)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count()) return;
    Upload &u = d->m_uploads[row];
    u.removeTag(tag);
    QModelIndex modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex);
}

int UploadModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const UploadModel);
    Q_UNUSED(parent);
    return d->m_uploads.count();
}

QVariant UploadModel::data(const QModelIndex &index, int role) const
{
    Q_D(const UploadModel);

    int row = index.row();
    if (row < 0 || row >= d->m_uploads.count()) return QVariant();

    const Upload &u = d->m_uploads[row];
    switch (role) {
    case UrlRole:
        return u.url();
    case ProgressRole:
        return u.progress();
    case StatusRole:
        return u.status();
    case TitleRole:
        return u.title();
    case DescriptionRole:
        return u.description();
    case TagsRole:
        return u.tags();
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> UploadModel::roleNames() const
{
    Q_D(const UploadModel);
    return d->m_roles;
}

#include "upload_model.moc"
