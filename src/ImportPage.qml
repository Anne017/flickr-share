import Ubuntu.Components 1.1
import Ubuntu.Content 0.1

Page {
    visible: false

    ContentPeerPicker {
        id: root

        handler: ContentHandler.Source
        contentType: ContentType.Pictures

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Multiple;
            peer.request();
            pageStack.pop()
        }

        onCancelPressed: pageStack.pop()
    }
}
