/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.1

ListView {
    id: root

    property var accounts

    spacing: 2
    header: Button {
        anchors.horizontalCenter: parent.horizontalCenter
        enabled: !model.busy
        text: model.busy ?
            i18n.tr("%1 uploaded, %2 remaining").arg(model.completedCount).arg(model.count) :
            i18n.tr("Publish %1 item as %2", "Publish %1 items as %2", model.count).arg(model.count).arg(accounts.accountName)
        onClicked: model.startUpload()
    }

    delegate: ImageDelegate {
        anchors { left: parent.left; right: parent.right }
        height: units.gu(14)
        url: model.url
        title: model.title
        description: model.description
        tags: model.tags
        uploadStatus: model.status
        progress: model.progress
        onClicked: pageStack.push(Qt.resolvedUrl("EditPage.qml"), {
            "modelData": model,
            "model": root.model,
            "index": index,
        })
    }
}
