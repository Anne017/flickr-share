/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UPLOADER_SETTINGS_H
#define UPLOADER_SETTINGS_H

#include <QObject>

namespace Uploader {

class SettingsPrivate;
class Settings: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isPublic READ isPublic WRITE setPublic \
               NOTIFY isPublicChanged)
    Q_PROPERTY(bool isFriends READ isFriends WRITE setFriends \
               NOTIFY isFriendsChanged)
    Q_PROPERTY(bool isFamily READ isFamily WRITE setFamily \
               NOTIFY isFamilyChanged)
    Q_PROPERTY(bool isHidden READ isHidden WRITE setHidden \
               NOTIFY isHiddenChanged)

public:
    Settings(QObject *parent = 0);
    ~Settings();

    void setPublic(bool value);
    bool isPublic() const;

    void setFriends(bool value);
    bool isFriends() const;

    void setFamily(bool value);
    bool isFamily() const;

    void setHidden(bool value);
    bool isHidden() const;

Q_SIGNALS:
    void isPublicChanged();
    void isFriendsChanged();
    void isFamilyChanged();
    void isHiddenChanged();

private:
    SettingsPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Settings)
};

} // namespace

#endif // UPLOADER_SETTINGS_H
