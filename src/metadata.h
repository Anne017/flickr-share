/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_METADATA_H
#define IMAGINARIO_METADATA_H

#include "types.h"

#include <QDateTime>
#include <QObject>
#include <QStringList>
#include <QUrl>

namespace Imaginario {

class MetadataPrivate;
class Metadata: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool embed READ embed WRITE setEmbed NOTIFY embedChanged)

public:
    /* This contains the bits of data which we always read when importing
     * items */
    struct ImportData {
        QString title;
        QString description;
        QDateTime time;
        QStringList tags;
        int rating;
        GeoPoint location;
    };

    Metadata(QObject *parent = 0);
    ~Metadata();

    void setEmbed(bool embed);
    bool embed() const;

    Q_INVOKABLE void writeTags(const QString &file, const QStringList &tags);
    Q_INVOKABLE void writeRating(const QString &file, int rating);

    bool readImportData(const QString &file, ImportData &data);

Q_SIGNALS:
    void embedChanged();

private:
    MetadataPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Metadata)
};

} // namespace

#endif // IMAGINARIO_METADATA_H
