/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Flickr 1.0
import QtQuick 2.0
import Ubuntu.Components 1.1

Item {
    id: root

    property alias url: image.source
    property string title: ""
    property string description: ""
    property alias tags: metadata.tags
    property var uploadStatus: UploadModel.Ready
    property int progress: -1

    signal clicked

    states: [
        State {
            name: "ready"
            when: uploadStatus == UploadModel.Ready
        },
        State {
            name: "uploading"
            when: uploadStatus == UploadModel.Uploading
            PropertyChanges { target: mouseArea; enabled: false }
            PropertyChanges { target: metadata; opacity: 0.3 }
            PropertyChanges { target: progressBar; opacity: 1.0 }
            PropertyChanges { target: progressText; opacity: 1.0 }
        },
        State {
            name: "uploaded"
            extend: "uploading"
            when: uploadStatus == UploadModel.Uploaded
            PropertyChanges { target: progressText; opacity: 0.0 }
            PropertyChanges { target: progressText; opacity: 0.0 }
        },
        State {
            name: "failed"
            extend: "ready"
            when: uploadStatus == UploadModel.Failed
        }
    ]

    transitions: [
        Transition {
            PropertyAnimation {
                duration: 500
                easing.type: Easing.InOutQuad
                property: "opacity"
            }
        }
    ]

    Image {
        id: image
        anchors{ left: parent.left; top: parent.top; bottom: parent.bottom }
        width: height
        fillMode: Image.PreserveAspectCrop
        /* We are cropping the image, so we need higher resolution: */
        sourceSize.width: width * 2
        sourceSize.height: height * 2
    }

    ImageInfo {
        id: metadata
        anchors {
            left: image.right; right: parent.right; top: parent.top; bottom: parent.bottom
            leftMargin: units.gu(1)
        }
        title: root.title
        description: root.description
        tags: root.tags
    }

    Rectangle {
        id: progressBar
        anchors { left: metadata.left; bottom: parent.bottom }
        opacity: 0.0
        width: metadata.width * progress / 100
        height: units.gu(0.5)
        color: "#ff0084"
    }

    Label {
        id: progressText
        anchors.centerIn: metadata
        opacity: 0.0
        text: i18n.tr("%1%").arg(progress)
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.clicked()
    }
}
