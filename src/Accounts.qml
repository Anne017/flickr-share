/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0
import Ubuntu.OnlineAccounts 0.1
import Ubuntu.OnlineAccounts.Client 0.1

Item {
    id: root

    property alias accountName: chosenAccount.displayName
    property bool authenticated: false
    property string consumerKey: ""
    property string consumerSecret: ""
    property string token: ""
    property string tokenSecret: ""

    function choose() {
        PopupUtils.open(dialogComponent)
    }

    Component.onCompleted: {
        if (accounts.count === 0) {
            choose()
        } else {
            chosenAccount.objectHandle = accounts.get(0, "accountServiceHandle")
        }
    }

    AccountServiceModel {
        id: accounts
        applicationId: "it.mardy.uploader_uploader"
    }

    Setup {
        id: setup
        applicationId: accounts.applicationId
        providerId: "flickr"
    }

    AccountService {
        id: chosenAccount
        onObjectHandleChanged: authenticate(null)
        onAuthenticated: {
            root.consumerKey = authData.parameters.ConsumerKey
            root.consumerSecret = authData.parameters.ConsumerSecret
            root.token = reply.AccessToken
            root.tokenSecret = reply.TokenSecret
            root.authenticated = true
        }
        onAuthenticationError: {
            console.warn("Authentication error: " + error.code)
            root.authenticated = false
        }
    }

    Component {
        id: dialogComponent
        Dialog {
            id: dialog
            title: i18n.tr("Choose a Flickr Account")

            Repeater {
                model: accounts
                ListItem.Standard {
                    anchors { left: parent.left; right: parent.right }
                    height: units.gu(6)
                    text: model.displayName
                    onClicked: {
                        chosenAccount.objectHandle = model.accountServiceHandle
                        PopupUtils.close(dialog)
                    }
                }
            }

            Label {
                anchors {
                    left: parent.left; right: parent.right
                    margins: units.gu(1)
                }
                visible: accounts.count === 0
                text: i18n.tr("No Flickr accounts available. Tap on the button below to add an account.")
                wrapMode: Text.Wrap
            }

            Button {
                text: i18n.tr("Add a new account")
                onClicked: setup.exec()
            }

            Button {
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }
        }
    }
}

