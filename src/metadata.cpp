/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "metadata.h"

#include <QDebug>
#include <QHash>
#include <QMutex>
#include <QtConcurrent>
#include <exiv2/convert.hpp>
#include <exiv2/image.hpp>
#include <exiv2/xmpsidecar.hpp>
#include <sys/stat.h>
#include <utime.h>

using namespace Imaginario;

namespace Imaginario {

static const Exiv2::Value *lookUp(const Exiv2::Image &image,
                                  const char * const *keys)
{
    for (; *keys != NULL; keys++) {
        const char *key = *keys;
        if (key[0] == 'X') {
            const Exiv2::XmpData &xmpData = image.xmpData();
            Exiv2::XmpData::const_iterator i =
                xmpData.findKey(Exiv2::XmpKey(key));
            if (i != xmpData.end()) {
                return &i->value();
            }
        } else if (key[0] == 'E') {
            const Exiv2::ExifData &exifData = image.exifData();
            Exiv2::ExifData::const_iterator i =
                exifData.findKey(Exiv2::ExifKey(key));
            if (i != exifData.end()) {
                return &i->value();
            }
        } else if (key[0] == 'I') {
            const Exiv2::IptcData &iptcData = image.iptcData();
            Exiv2::IptcData::const_iterator i =
                iptcData.findKey(Exiv2::IptcKey(key));
            if (i != iptcData.end()) {
                return &i->value();
            }
        }
    }

    return 0;
}

static inline QString title(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Xmp.dc.title",
        "Iptc.Application2.ObjectName",
        "Iptc.Application2.Subject",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    return v ? QString::fromStdString(v->toString(0)) : QString();
}

static inline QString description(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Xmp.dc.description",
        "Exif.Photo.UserComment",
        "Exif.Image.ImageDescription",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    return v ? QString::fromStdString(v->toString(0)) : QString();
}

static inline QDateTime creationData(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Exif.Photo.DateTimeOriginal",
        "Xmp.exif.DateTimeOriginal",
        "Xmp.xmp.CreateDate",
        "Exif.Photo.DateTimeDigitized",
        "Xmp.exif.DateTimeDigitized",
        "Exif.Image.DateTime",
        "Xmp.tiff.DateTime",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    if (v) {
        QString timeString = QString::fromStdString(v->toString());
        return QDateTime::fromString(timeString, "yyyy:MM:dd HH:mm:ss");
    } else {
        return QDateTime();
    }
}

static inline QStringList keywords(const Exiv2::Image &image)
{
    QStringList keywords;

    static const char *const keys[] = {
        "Xmp.dc.subject",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    if (!v) return keywords;

    int n = v->count();
    for (int j = 0; j < n; j++) {
        keywords.append(QString::fromStdString(v->toString(j)));
    }

    return keywords;
}

static inline int rating(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Xmp.xmp.Rating",
        NULL
    };
    const Exiv2::Value *v = lookUp(image, keys);
    return v ? int(v->toLong()) : -1;
}

struct GeoCoord {
    GeoCoord(const Exiv2::Value &v): positive(true) {
        int count = v.count();
        coord[0] = count > 0 ? v.toFloat(0) : NAN;
        coord[1] = count > 1 ? v.toFloat(1) : 0;
        coord[2] = count > 2 ? v.toFloat(2) : 0;
    }
    GeoCoord(const QStringList &parts): positive(true) {
        int count = parts.count();
        coord[0] = count > 0 ? parts[0].toFloat() : NAN;
        coord[1] = count > 1 ? parts[1].toFloat() : 0;
        coord[2] = count > 2 ? parts[2].toFloat() : 0;
    }
    void setPositive(bool p) { positive = p; }
    Geo toGeo() {
        Geo sum = coord[0] + coord[1] / 60.0 + coord[2] / 3600.0;
        return positive ? sum : -sum;
    }
private:
    Geo coord[3];
    bool positive;
};

static Geo parseXmpGeoCoord(const QString &s)
{
    if (s.isEmpty()) return 200.0;
    QChar reference = s.at(s.length() - 1).toUpper();
    QStringList coords = s.left(s.length() -1).split(',');
    GeoCoord c(coords);
    if (reference == 'W' || reference == 'S') {
        c.setPositive(false);
    }
    return c.toGeo();
}

static inline GeoPoint readExifGeoLocation(const Exiv2::ExifData &exifData)
{
    Exiv2::ExifData::const_iterator i =
        exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLatitude"));
    if (i == exifData.end()) return GeoPoint();
    GeoCoord lat(i->value());

    i = exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLatitudeRef"));
    if (i != exifData.end() &&
        i->value().toString().compare(0, 1, "S") == 0) {
        lat.setPositive(false);
    }

    i = exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLongitude"));
    if (i == exifData.end()) return GeoPoint();
    GeoCoord lon(i->value());

    i = exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLongitudeRef"));
    if (i != exifData.end() &&
        i->value().toString().compare(0, 1, "W") == 0) {
        lon.setPositive(false);
    }

    return GeoPoint(lat.toGeo(), lon.toGeo());
}

static inline GeoPoint readGeoLocation(const Exiv2::Image &image)
{
    const Exiv2::XmpData &xmpData = image.xmpData();
    Exiv2::XmpData::const_iterator i =
        xmpData.findKey(Exiv2::XmpKey("Xmp.exif.GPSLatitude"));
    if (i != xmpData.end()) {
        QString lat = QString::fromStdString(i->value().toString());
        i = xmpData.findKey(Exiv2::XmpKey("Xmp.exif.GPSLongitude"));
        if (i != xmpData.end()) {
            QString lon = QString::fromStdString(i->value().toString());
            return GeoPoint(parseXmpGeoCoord(lat), parseXmpGeoCoord(lon));
        }
    }

    /* Else, read the location from the EXIF data */
    return readExifGeoLocation(image.exifData());
}

class MetadataPrivate
{
public:
    struct Changes {
        Changes(const QString &fileName):
            orphaned(false), fileName(fileName), fields(None) {}
        enum Fields {
            None = 0,
            Tags = 1 << 0,
            Rating = 1 << 1,
        };
        QMutex mutex;
        bool orphaned;
        QString fileName;
        int fields;
        QStringList tags;
        int rating;
    };
    typedef Changes* ChangesP;

    MetadataPrivate(Metadata *q);
    ~MetadataPrivate();

    static bool writeChanges(bool embed, Changes *changes);

    void writeTags(const QString &file, const QStringList &tags);
    void writeRating(const QString &file, int rating);

private:
    friend class Metadata;
    bool m_embed;
    QHash<QString,ChangesP> m_changes;
};

} // namespace

MetadataPrivate::MetadataPrivate(Metadata *):
    m_embed(false)
{
}

MetadataPrivate::~MetadataPrivate()
{
    Q_FOREACH(Changes *changes, m_changes) {
        changes->mutex.lock();
        bool mustDelete;
        if (changes->fields == Changes::None) {
            // Already executed, can be deleted
            mustDelete = true;
        } else {
            mustDelete = false;
            changes->orphaned = true;
        }
        changes->mutex.unlock();
        if (mustDelete) delete changes;
    }
}

static bool imageIsWritable(const QString &file)
{
    try {
        int type = Exiv2::ImageFactory::getType(file.toUtf8().constData());
        Exiv2::AccessMode mode =
            Exiv2::ImageFactory::checkMode(type, Exiv2::mdXmp);
        return mode == Exiv2::amWrite || mode == Exiv2::amReadWrite;
    } catch (Exiv2::AnyError &e) {
        qDebug() << "Exiv2 exception, code" << e.code();
        return false;
    }
}

static bool saveMetadata(Exiv2::Image *image)
{
    std::string fileName = image->io().path();
    struct stat statBefore;
    stat(fileName.c_str(), &statBefore);

    try {
        image->writeMetadata();
    } catch (Exiv2::AnyError &e) {
        qCritical() << "Exiv2 exception, code" << e.code();
        return false;
    }

    /* preserve modification time */
    struct stat statAfter;
    stat(fileName.c_str(), &statAfter);
    struct utimbuf times;
    times.actime = statAfter.st_atime;
    times.modtime = statBefore.st_mtime;
    utime(fileName.c_str(), &times);

    return true;
}

bool MetadataPrivate::writeChanges(bool embed, Changes *changes)
{
    QString destFile;
    bool useSidecar;

    changes->mutex.lock();
    const QString &fileName = changes->fileName;

    if (embed && imageIsWritable(fileName)) {
        destFile = fileName;
        useSidecar = false;
    } else {
        destFile = fileName + ".xmp";
        useSidecar = true;
    }

    Exiv2::Image::AutoPtr image;

    try {
        image = Exiv2::ImageFactory::open(destFile.toUtf8().constData());
        image->readMetadata();
    } catch (Exiv2::AnyError &e) {
        if (useSidecar) {
            // The sidecar could not be opened, create it from the image
            try {
                Exiv2::Image::AutoPtr source =
                    Exiv2::ImageFactory::open(fileName.toUtf8().constData());
                source->readMetadata();
                image = Exiv2::ImageFactory::create(Exiv2::ImageType::xmp,
                                                    destFile.toUtf8().constData());
                image->setMetadata(*source);
            } catch (Exiv2::AnyError &e) {
                qDebug() << "Sidecar creation failed" << e.what();
                changes->mutex.unlock();
                return false;
            }
        } else {
            qDebug() << "Exiv2 exception, code" << e.what();
            changes->mutex.unlock();
            return false;
        }
    }

    Exiv2::XmpData &xmpData = image->xmpData();

    if (changes->fields & Changes::Tags) {
        // Clear the current tags
        Exiv2::XmpData::iterator i =
            xmpData.findKey(Exiv2::XmpKey("Xmp.dc.subject"));
        if (i != xmpData.end()) { xmpData.erase(i); }
        // Write the new tags
        Q_FOREACH(const QString &tag, changes->tags) {
            xmpData["Xmp.dc.subject"] = tag.toStdString();
        }
    }

    if (changes->fields & Changes::Rating) {
        if (changes->rating >= 0) {
            xmpData["Xmp.xmp.Rating"] = changes->rating;
        } else {
            Exiv2::XmpData::iterator i =
                xmpData.findKey(Exiv2::XmpKey("Xmp.xmp.Rating"));
            if (i != xmpData.end()) { xmpData.erase(i); }
        }
    }

    bool ok = saveMetadata(image.get());
    if (changes->orphaned) {
        changes->mutex.unlock();
        delete changes;
    } else if (Q_LIKELY(ok)) {
        changes->fields = Changes::None;
        changes->mutex.unlock();
    }

    return ok;
}

void MetadataPrivate::writeTags(const QString &file, const QStringList &tags)
{
    ChangesP &changes = m_changes[file];
    if (changes == 0) {
        changes = new Changes(file);
    }
    changes->mutex.lock();
    bool newlyCreated = (changes->fields == Changes::None);
    changes->fields |= Changes::Tags;
    changes->tags = tags;
    changes->mutex.unlock();
    if (newlyCreated) {
        QtConcurrent::run(&MetadataPrivate::writeChanges,
                          m_embed, changes);
    }
}

void MetadataPrivate::writeRating(const QString &file, int rating)
{
    ChangesP &changes = m_changes[file];
    if (changes == 0) {
        changes = new Changes(file);
    }
    changes->mutex.lock();
    bool newlyCreated = (changes->fields == Changes::None);
    changes->fields |= Changes::Rating;
    changes->rating = rating;
    changes->mutex.unlock();
    if (newlyCreated) {
        QtConcurrent::run(&MetadataPrivate::writeChanges,
                          m_embed, changes);
    }
}

Metadata::Metadata(QObject *parent):
    QObject(parent),
    d_ptr(new MetadataPrivate(this))
{
}

Metadata::~Metadata()
{
    delete d_ptr;
}

void Metadata::setEmbed(bool embed)
{
    Q_D(Metadata);
    if (embed == d->m_embed) return;
    d->m_embed = embed;
    Q_EMIT embedChanged();
}

bool Metadata::embed() const
{
    Q_D(const Metadata);
    return d->m_embed;
}

void Metadata::writeTags(const QString &file, const QStringList &tags)
{
    Q_D(Metadata);
    return d->writeTags(file, tags);
}

void Metadata::writeRating(const QString &file, int rating)
{
    Q_D(Metadata);
    return d->writeRating(file, rating);
}

bool Metadata::readImportData(const QString &file, ImportData &data)
{
    Exiv2::Image::AutoPtr image;

    /* If an XMP sidecar file exists, read it */
    QString sidecar = file + ".xmp";
    try {
        image = Exiv2::ImageFactory::open(sidecar.toUtf8().constData());
        image->readMetadata();
    } catch (Exiv2::AnyError &e) {
        try {
            image = Exiv2::ImageFactory::open(file.toUtf8().constData());
            image->readMetadata();
        } catch (Exiv2::AnyError &e) {
            qDebug() << "Exiv2 exception, code" << e.code();
            return false;
        }
    }

    data.title = title(*image);
    data.description = description(*image);
    data.time = creationData(*image);
    data.tags = keywords(*image);
    data.rating = rating(*image);
    data.location = readGeoLocation(*image);

    return true;
}
